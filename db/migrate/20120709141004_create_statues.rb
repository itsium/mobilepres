class CreateStatues < ActiveRecord::Migration
  def change
    create_table :statues do |t|
      t.string :name

      t.timestamps
    end
    
    Statue.create :name => 'Private'
    Statue.create :name => 'Public'
    Statue.create :name => 'PreCommand'
  end
end
