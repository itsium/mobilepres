class ProductsController < ApplicationController
  def index
    min = 0
    count = 50
    if params[:min] then min = params[:min].to_i end
    if params[:count] then count = params[:count].to_i end
    if params[:statue_id] then statue_id = params[:statue_id].to_i end
    if params[:cat_id] then col_id = params[:cat_id].to_i end
    @products = Product.offset(min).limit(count)
    if col_id
      @products = @products.where(:collection_id => col_id) if col_id
    end
    if statue_id != 0
      @products = @products.where(:statue_id => statue_id) if statue_id
    end
  end

  def show
    @product = Product.find(params[:id])
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(params[:product])
    if @product.save
      redirect_to @product, :notice => "Successfully created product."
    else
      render :action => 'new'
    end
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    if @product.update_attributes(params[:product])
      redirect_to @product, :notice  => "Successfully updated product."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    redirect_to products_url, :notice => "Successfully destroyed product."
  end
end
