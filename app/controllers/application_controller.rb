class ApplicationController < ActionController::Base
  has_mobile_fu
  #protect_from_forgery
  before_filter :verif_auth

  def mobile_device?
    if session[:mobile_param]
      session[:mobile_param] == "1"
    else
      request.user_agent =~ /Mobile|webOS|Android|AppleWebKit/
    end
  end

  helper_method :mobile_device?

  def verif_auth
    session[:mobile_param] = (params[:controller] =~ /^admin\/.*|^active_admin\//) ? "0" : "1"
    request.format = :mobile if mobile_device?
  end

end
