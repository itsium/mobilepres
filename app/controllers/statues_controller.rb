class StatuesController < ApplicationController
  def index
    @statues = Statue.all
  end

  def show
    @statue = Statue.find(params[:id])
    @products = @statue.products
  end

  def new
    @statue = Statue.new
  end

  def create
    @statue = Statue.new(params[:statue])
    if @statue.save
      redirect_to @statue, :notice => "Successfully created statue."
    else
      render :action => 'new'
    end
  end

  def edit
    @statue = Statue.find(params[:id])
  end

  def update
    @statue = Statue.find(params[:id])
    if @statue.update_attributes(params[:statue])
      redirect_to @statue, :notice  => "Successfully updated statue."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @statue = Statue.find(params[:id])
    @statue.destroy
    redirect_to statues_url, :notice => "Successfully destroyed statue."
  end

  def list
    @statues = Statue.all
  end
end
