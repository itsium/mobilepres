collections = []
@collections.each do |c|
  col = {:id => c.id, :label => c.name, :urlId => c.name.downcase.gsub(/([^\d\w])/, "-")}
  if c.products.length > 0
    col[:imageUrl] = c.products.first.photos.first.image.url
    collections << col
  end
end
json.total collections.length
json.results collections
json.success true