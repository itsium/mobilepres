length = @products.length
if length < 8
  length = 8
end
json.total length

ps = []
@products.each do |p|
  result = {}
  result[:id] = p.id
  result[:name] = p.name
  result[:description] = p.description[0,250] + ' ...'
  result[:urlId] = p.collection.name.downcase.gsub(/([^\d\w])/, "-") + "/" + p.name.downcase.gsub(/([^\d\w])/, "-")

  imgs = []
  photo = p.photos.first
  thumb = {:sizeName => "Thumb", :url => photo.image.thumb.url}
  big = {:sizeName => "Big", :url => photo.image.big.url}
  imgs.push thumb
  imgs.push big
  result[:images] = imgs
  ps << result
end

i = 0
if @products.length > 0
  while ps.length < 8
    @products.each do |p|
      ps << {:id => 999999 + i + p.id, :name => "", :images => [], :urlId => "collection/" + p.collection.name.downcase}
      i = i + 1
    end
  end
end
json.results ps
