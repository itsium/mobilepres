json.total @statues.length
results = [{:id => 0, :name => "All"}]
@statues.each do |s|
  results << {:id => s.id, :name => s.name}
end
json.results results