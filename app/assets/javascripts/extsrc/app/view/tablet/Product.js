Ext.define('App.view.tablet.Product', {
    extend: 'Ext.Container',
    xtype: 'product',

    requires: ['Ext.Img'],

    config: {
        baseCls: 'product-view',
        layout: {
            type: 'hbox'
        },
        items: [{
            xtype: 'image',
            flex: 1
        }, {
            flex: 2,
            id: 'description',
            cls: 'description',
            scrollable: true,
            tpl: new Ext.XTemplate(
                '<div class="name">{name}</div>',
                '<div class="text">{description}</div>',
                '<a href="{url}" target="_block">Buy Now</a>'
            )
        }]
    },

    initialize: function() {
        var image = this.down('image');
        image.on({
            scope: this,
            load: function() {
                image.element.dom.style.backgroundSize = "contain";
            }
        });
    },

    updateData: function(newData) {
        var image = this.down('image');
        image.element.dom.style.backgroundSize = "30%";
        image.element.dom.style.backgroundImage = 'url(/assets/sencha/default/loading.gif)';
        image.setSrc('');
        image.setSrc(newData.images.big);
        Ext.getCmp('description').setData(newData);
    }
});
