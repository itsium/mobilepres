Ext.define('App.view.tablet.Main', {
    extend: 'App.view.Main',

    requires: [
        'App.view.ProductsList',
        'App.view.tablet.Product'
    ]
});
