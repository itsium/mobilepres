Ext.define('App.view.Main', {
    extend: 'Ext.Panel',
    xtype: 'main',

    requires: [
	'App.store.Statues'
    ],

    config: {
        fullscreen: true,
	layout: 'card',
	items: [{
	    xtype: 'categories'
	}, {
	    xtype: 'productslist'
	}, {
	    xtype: 'product'
	}, {
	    docked: 'top',
	    xtype: 'titlebar',
	    ui: 'dark',
	    items: [{
		xtype: 'button',
		align: 'left',
		ui: 'dark',
		id: 'backbutton',
		text: 'Back',
		hidden: true
	    }, {
		xtype: 'selectfield',
		ui: 'dark',
		id: 'statusselectfield',
		align: 'right',
		label: 'Status',
		hidden: true,
		displayField: 'name',
		valueField: 'id',
		value: 0,
		store: {
		    xclass: 'App.store.Statues',
		    autoLoad: true
		}
	    }]
	}]
    }
});
