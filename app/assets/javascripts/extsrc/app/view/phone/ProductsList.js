Ext.define('App.view.phone.ProductsList', {
    extend: 'App.view.ProductsList',
    requires: ['App.view.phone.Products'],

    config: {
        count: 1,

        innerItemConfig: {
            xclass: 'App.view.phone.Products'
        },

        directionLock: true
    }
});
