Ext.define('App.view.Categories', {
    extend: 'Ext.dataview.DataView',
    xtype: 'categories',
    requires: [
	'App.store.Categories'
    ],

    config: {
	title: 'Categories',
        baseCls: 'categories-list',
        itemTpl: [
            '<div class="image" style="background-image:url({imageUrl}); background-size: cover;"></div>',
            '<div class="name">{label}</div>'
        ].join('')
    },

    onItemTap: function(container, target, index, e) {
        var record = this.getStore() && this.getStore().getRange()[index];
        this.fireEvent('itemtap', this, index, target, record, e);
    }
});
