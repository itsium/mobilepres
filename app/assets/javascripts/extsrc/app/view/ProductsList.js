Ext.define('App.view.ProductsList', {
    extend: 'Ext.carousel.Carousel',
    xtype: 'productslist',
    requires: ['App.view.Products'],

    config: {
	title: 'Products',
        direction: 'horizontal',
	indicator: false,
        innerItemConfig: {
            xclass: 'App.view.Products'
        },
        count: 8,
        offsetLimit: 50,
        store: null
    },

    initialize: function() {
        Ext.Viewport.on('orientationchange', this.onOrientationChange, this);

        this.element.on({
            scope: this,
            tap: 'onTap'
        });
    },

    onTap: function(e) {
        var element = Ext.get(e.target),
            store = this.store,
            id;

        if (!element.hasCls('product')) {
            element = Ext.get(e.target).parent('.product');
        }

        id = Math.abs(element.getAttribute('ref'));
        record = store.getAt(store.findExact('id', id));
        if (record) {
            this.fireEvent('itemtap', this, record);
        }
    },

    onOrientationChange: function(vewport, orientation) {
        this.refreshItems();
    },

    updateStore: function(newStore) {
        var me = this;
	me.store = newStore;

        if (newStore.isLoading()) {
            me.setMasked({
                xtype: 'loadmask'
            });

            newStore.on('load', function() {
                me.setMasked(false);

                me.updateStore(newStore);
            }, me, {
                single: true
            });
        } else {
	    var items = [];
	    var length = newStore.getCount();
	    if ((length / 8.0) < 1.0) {
		length = 1;
	    } else {
		length = length / 8;
	    }
	    for (var i = 0; i < length; ++i) {
		items.push({records: newStore.getRange(),
			    xclass: 'App.view.Products'});
	    }
	    me.setItems(items);
            me.setActiveItem(0);
	}
    }
});
