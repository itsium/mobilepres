Ext.define('App.controller.Category', {
    extend: 'Ext.app.Controller',
    requires: [
	'App.store.Categories',
	'App.store.Products',
	'App.view.Categories'
    ],
    config: {
        refs: {
            main: 'main',
	    statusSelectField: '#statusselectfield',
	    categories: 'categories',
	    productsList: 'productslist',
	    product: 'product',
	    backButton: '#backbutton'
        },
        before: {
            showCategoryById: 'ensureStoreLoad',
	    showProductById: 'ensureStoreLoad'
        },
        control: {
	    backButton: {
		tap: 'onBackButtonTap'
	    },
            main: {
                activeitemchange: 'onMainActiveItemChange'
            },
            categories: {
                itemtap: 'onCategoryTap'
            },
            productslist: {
                itemtap: 'onProductTap'
            },
	    statusSelectField: {
		change: 'onStatusSelectFieldChange'
	    }
        },
        routes: {
            '': 'showCategories',
	    ':category_id': 'showCategoryById',
	    ':category_id/:product_id': 'showProductById'
        },
        currentRecord: null
    },

    onBackButtonTap: function() {
	this.directionSlide = 'right';
	if (Ext.isDefined(this.activeItem) === false) {
	    this.activeItem = 0;
	}
	if (this.activeItem == 1) {
	    this.showCategories();
	} else if (this.activeItem == 2) {
	    this.showCategoryById(this.currentCategoryId);
	}
    },

    onMainActiveItemChange: function(container, item, oldItem) {
	this.activeItem = 0;
	var length = container.getItems().length;
	for (var i = 0; i < length; ++i) {
	    if (item == container.getAt(i)) {
		this.activeItem = i;
		break;
	    }
	}
	if (this.activeItem > 0) {
	    this.getBackButton().show();
	} else {
	    this.getBackButton().hide();
	}
	if (this.activeItem == 1) {
	    this.getStatusSelectField().show();
	} else {
	    this.getStatusSelectField().hide();
	}
    },

    init: function() {
	this.currentCategoryId = '';
	this.currentProductId = '';
	this.categoryStore = Ext.create('App.store.Categories', {
	    autoLoad: true
	});
	this.productStore = Ext.create('App.store.Products', {
	    listeners: {
		scope: this,
		beforeload: function() {
		    this.getProductsList().setMasked({
			xtype: 'loadmask'
		    });
		},
		load: function() {
		    this.getProductsList().setMasked(false);
		}
	    }
	});
    },

    onStatusSelectFieldChange: function(select, newValue, oldValue) {
	var record = this.getCurrentRecord();
	if (Ext.isObject(record)) {
	    this.showProducts(this.getCurrentRecord());
	}
    },

    ensureStoreLoad: function(action) {
        var store = this.categoryStore;
        if (store.data.all.length) {
            action.resume();
        } else {
            store.on('load', function() {
                action.resume();
            }, this, {
                single: true
            });
        }
    },

    showCategories: function() {
	if (this.getStatusSelectField().isHidden() === false) {
	    this.getStatusSelectField().hide();
	}
	this.getCategories().setStore(this.categoryStore);
	this.getMain().animateActiveItem(this.getCategories(), {
	    type: 'slide',
	    direction: this.directionSlide || 'left'
	});
	delete this.directionSlide;
    },

    onCategoryTap: function(view, index, target, record, e) {
        this.redirectTo(record);
    },

    loadProducts: function() {
	var category_record = this.getCategoryRecordFromUrl(this.currentCategoryId);
	if (Ext.isObject(category_record) === false) {
	    Ext.Logger.warn('Category not found');
	    return;
	}
	this.productStore.getProxy().setExtraParam('cat_id', category_record.get('id'));
	this.productStore.getProxy().setExtraParam('statue_id', this.getStatusSelectField().getValue());
	this.productStore.load({
	    callback: this.loadProduct,
	    scope: this
	});
    },

    loadProduct: function() {
	var url = this.currentCategoryId + '/' + this.currentProductId;
	var productRecord = this.getProductRecordFromUrl(url);
	if (Ext.isObject(productRecord) === false) {
	    Ext.Logger.warn('Product not found');
	    return;
	}
	this.getProduct().setData(productRecord.data);
	this.setCurrentRecord(productRecord);
	this.getMain().animateActiveItem(this.getProduct(), {
	    type: 'slide',
	    direction: this.directionSlide || 'left'
	});
	delete this.directionSlide;
    },

    showProductById: function(category_id, product_id) {
	this.currentCategoryId = category_id;
	this.currentProductId = product_id;
	var product_record = this.getProductRecordFromUrl(category_id + '/' + product_id);
	if (Ext.isObject(product_record) === false) {
	    this.loadProducts();
	} else {
	    this.loadProduct();
	}
    },

    getProductRecordFromUrl: function(url) {
	var productRecord = Ext.Array.filter(this.productStore.data.all, function(record) {
	    if (record.get('urlId') == url) {
		return record;
	    }
	}, this);
	return productRecord[0];
    },

    getCategoryRecordFromUrl: function(url) {
	var categoryRecord = Ext.Array.filter(this.categoryStore.data.all, function(record) {
	    if (record.get('urlId') == url) {
		return record;
	    }
	}, this);
	return categoryRecord[0];
    },

    showCategoryById: function(category_id) {
        var record = this.getCategoryRecordFromUrl(category_id);
        if (Ext.isObject(record) === false) {
            Ext.Logger.warn('Category not found');
	    return;
        }
        this.showProducts(record);
    },

    showProducts: function(record) {
	this.currentCategoryId = record.get('urlId');
	if (this.getStatusSelectField().isHidden() === true) {
	    this.getStatusSelectField().show();
	}
        this.setCurrentRecord(record);
	this.productStore.getProxy().setExtraParam('cat_id', record.get('id'));
	this.productStore.getProxy().setExtraParam('statue_id', this.getStatusSelectField().getValue());
        this.productStore.load({
	    callback: function() {
		this.getProductsList().updateStore(this.productStore);
	    },
	    scope: this
	});
	this.getMain().animateActiveItem(this.getProductsList(), {
	    type: 'slide',
	    direction: this.directionSlide || 'left'
	});
	delete this.directionSlide;
    },

    /**
     * Called when an item is tapped on.
     * This is overridden in the Tablet controller
     */
    onProductTap: Ext.emptyFn,

    /**
     * This creates and returns a new categories view, for when it is needed.
     * Ideally this should be improved at some point to only instansiate a max of 2 views
     * and then reuse the same views over again.
     * @param {Object} config The configuration for the view.
     * @return {App.view.Categories} view
     */
    getCategoriesView: function(config) {
        return Ext.create('App.view.Categories', config);
    },

    /**
     * This function is used to create and return a product view.
     * There is a different products view for both phone and tablet profiles, so we just have an emptyFn
     * in this base controller, and in the tablet/phone controller we will override this.
     * @param {Object} config The configuration for the view.
     * @return {Ext.Component} view
     */
    getProductsView: Ext.emptyFn,

    /**
     * This function is used to create and return a the product view.
     * There is a different product view for both phone and tablet profiles, so we just have an emptyFn
     * in this base controller, and in the tablet/phone controller we will override this.
     * @param {Object} config The configuration for the view.
     * @return {Ext.Component} view
     */
    getProductView: Ext.emptyFn
});
