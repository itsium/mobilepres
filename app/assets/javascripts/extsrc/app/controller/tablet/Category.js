Ext.define('App.controller.tablet.Category', {
    extend: 'App.controller.Category',

    config: {
        refs: {
            productView: {
                autoCreate: true,
                xtype: 'product',
                selector: 'product'
            }
        }
    },

    init: function() {
        this.callParent();

        //this.productsView = Ext.create('App.view.ProductsList');
        //this.productView = Ext.create('App.view.tablet.Product');
    },

    /**
     * @inherit
     */
    getProductsView: function() {
        return this.productsView;
    },

    /**
     * @inherit
     */
    getProductView: function() {
        return this.productView;
    },

    /**
     * @inherit
     */
    onProductTap: function(view, record) {
	this.redirectTo(record);
    }
});
