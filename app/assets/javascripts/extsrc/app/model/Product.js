Ext.define('App.model.Product', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            'id',
            'name',
            'description',
	    'urlId',
            'url', {
                name: 'images',
                convert: function(value, record) {
                    var ln = value.length;
                    var images = {};
                    for (i = 0; i < ln; i++) {
                        images[value[i].sizeName.toLowerCase()]  = value[i].url;
                    }
                    return images;
                }
            }
        ],

        proxy: {
            type: 'ajax',
            url: '/products?mobile=1',
            limitParam: 'count',
            startParam: 'min',
            pageParam: false,
            reader: {
                type: 'json',
                rootProperty: 'results',
		totalProperty: 'total'
            }
        }
    },

    toUrl: function() {
        return this.get('urlId');
    }
});
