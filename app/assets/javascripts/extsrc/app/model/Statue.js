Ext.define('App.model.Statue', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            'id',
            'name'
        ],
        proxy: {
            type: 'ajax',
            url: '/statues?mobile=1',
	    reader: {
		type: 'json',
		rootProperty: 'results',
		totalProperty: 'total'
	    }
	}
    }
});
