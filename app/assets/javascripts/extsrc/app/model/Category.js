Ext.define('App.model.Category', {
    extend: 'Ext.data.Model',
    requires: ['App.model.Product'],

    config: {
        fields: [
            'urlId',
            'label',
	    'imageUrl'
        ],
        proxy: {
            type: 'ajax',
            url: '/collections?mobile=1',
	    reader: {
		type: 'json',
		rootProperty: 'results',
		totalProperty: 'total'
	    }
        }
    },

    toUrl: function() {
        return this.get('urlId');
    }
});
