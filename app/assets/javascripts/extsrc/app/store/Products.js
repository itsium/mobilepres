Ext.define('App.store.Products', {
    extend: 'Ext.data.Store',
    requires: ['App.model.Product'],

    config :{
        model: 'App.model.Product',
	autoLoad: false
    }
});
