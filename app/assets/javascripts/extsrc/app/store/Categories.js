Ext.define('App.store.Categories', {
    extend: 'Ext.data.Store',
    requires: ['App.model.Category'],

    config :{
        model: 'App.model.Category'
    }
});
