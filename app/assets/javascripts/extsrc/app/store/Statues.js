Ext.define('App.store.Statues', {
    extend: 'Ext.data.Store',
    requires: ['App.model.Statue'],

    config :{
        model: 'App.model.Statue'
    }
});
