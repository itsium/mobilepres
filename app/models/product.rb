class Product < ActiveRecord::Base
  attr_accessible :name, :description, :collection_id, :statue_id, :photos_attributes
  belongs_to :statue
  belongs_to :collection
  has_many :photos
  accepts_nested_attributes_for :photos,  :allow_destroy => true, :reject_if => :all_blank
end
