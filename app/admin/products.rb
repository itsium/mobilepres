ActiveAdmin.register Product do
  menu :priority => 2
  filter :statue
  filter :collection
  filter :name
  filter :description
  filter :created_at
  
  scope :all, :default => true
  scope :Private do |products|
    products.where("statue_id = ?", Statue.where(:name => "Private").last.id)
  end
  scope :Public do |products|
    products.where("statue_id = ?", Statue.where(:name => "Public").last.id)
  end
  scope :PreCommand do |products|
    products.where("statue_id = ?", Statue.where(:name => "PreCommand").last.id)
  end
  
  index :as => :grid do |product|
    div :class => "image_cadre" do
      a :href => admin_product_path(product) do
          if product.photos.first.nil?
            image_tag("http://localhost:3000/assets/addimage.png", :class => "image_content")
          else
            image_tag(product.photos.first.image_url(:thumb).to_s, :class => "image_content")
          end
      end
    end
    a truncate(product.name), :href => admin_product_path(product)
    span "   | "
    a "Edit", :href => edit_admin_product_path(product)
    a link_to 'Destroy', admin_product_path(product),
                :confirm => 'Are you sure you want to delete this?', :method => :delete
    div truncate(product.description)
  end

  show do |product|
      attributes_table do 
        row :id
        row :name
        row :description
        row :collection
        row :statue
        row :created_at
        product.photos.each do |photo|
          row :image do
            image_tag(photo.image_url(:big).to_s)
          end
        end
      end
      
  end
  
  form(:html => { :multipart => true }) do |f|
    f.inputs "Product Details" do
      f.input :collection
      f.input :name
      f.input :statue
      f.input :description
    end
    
    f.inputs "Images" do
      f.has_many :photos do |s|
        s.input :image, :as => :file, 
                :hint => s.object.image_url.nil? ? 
                         s.template.content_tag(:span, "No Image Yet") : s.template.image_tag(s.object.image_url(:thumb).to_s)
        s.input :_destroy, :as=>:boolean, :required => false, :label => 'Remove image'
      end
    end
    
    f.buttons
  end
end
