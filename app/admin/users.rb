ActiveAdmin.register User do
  menu :priority => 1
  filter :email
  filter :created_at

  index do
    column :id, :sortable => :id do | user |
      link_to user.id, admin_user_path(user)
    end
    column :email, :sortable => :email do | user |
      link_to user.email, admin_user_path(user)
    end
    column :created_at
    column :last_sign_in_at
    default_actions
  end
  
  show do
    attributes_table  :id, :email, :sign_in_count, :current_sign_in_at, :last_sign_in_at, 
                      :last_sign_in_ip, :created_at, :updated_at
  end
  
  form do |f|
    f.inputs "User Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.buttons
  end
end
